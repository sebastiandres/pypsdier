.PHONY.: options
options:
	@echo "\tall\t\t clean, version, sdist and upload"
	@echo "\tclean\t\t removes pyc, rde, png files"
	@echo "\tversion\t\t ask for update the versioning files and commits changes"
	@echo "\tsdist\t\t makes the distribution"
	@echo "\tupload\t\t uploads the distribution to pypi"
	@echo "\tregister\t registers the package, needs to be done only once"

.PHONY.: all
all: clean version sdist upload

.PHONE. : clean
clean:
	@rm pypsdier/*/*.pyc
	@rm pypsdier/*/*.rde
	@rm pypsdier/*/*.png

.PHONE. : version
version:
	nano CHANGES.txt
	nano setup.py
	nano pypsdier/__init__.py
	git commit -a -m "Updating version previous to upload new dist"

.PHONY.: sdist
sdist:
	python setup.py sdist

.PHONY.: upload
upload: sdist
	python setup.py sdist upload

.PHONY.: register
register:
	python setup.py register

