===========
Pypsdier
===========

pypsdier is a library under heavy development for the simulation
and analysis of reaction-diffusion equations. 

It's still quite experimental, so be patient while a stable version comes out.

Main Site
=============
The main site for pypsdier is http://www.bitbucket.com/sebastiandres/pypsdier/.

Feel free to fork the code and play around, or explore the wiki and examples.

